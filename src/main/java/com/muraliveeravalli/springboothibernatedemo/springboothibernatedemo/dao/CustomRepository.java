package com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.dao;

import com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//@RepositoryRestResource(path = "members")
public interface CustomRepository extends JpaRepository<Employee, Integer> {
}
