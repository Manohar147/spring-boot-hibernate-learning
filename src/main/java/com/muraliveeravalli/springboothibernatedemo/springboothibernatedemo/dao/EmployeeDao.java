package com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.dao;

import com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.entity.Employee;

import java.util.List;

public interface EmployeeDao {

    List<Employee> findAll();

    Employee findById(final int id);

    void save(final Employee employee);

    void deleteById(final int id);

}
