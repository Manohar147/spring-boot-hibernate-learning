package com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.dao;

import com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.entity.Employee;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class EmployeeDaoHibernateImpl implements EmployeeDao {

    @Autowired
    private EntityManager entityManager;


    @Override
    public List<Employee> findAll() {
        Session session = entityManager.unwrap(Session.class);
        List<Employee> employees = session.createQuery("FROM Employee", Employee.class).getResultList();
        return employees;
    }

    @Override
    public Employee findById(int id) {
        Session session = entityManager.unwrap(Session.class);
        return session.get(Employee.class, id);
    }

    @Override
    public void save(Employee employee) {
        Session session = entityManager.unwrap(Session.class);
        session.saveOrUpdate(employee);
    }

    @Override
    public void deleteById(int id) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("DELETE  FROM Employee WHERE id=:employeeId");
        query.setParameter("employeeId", id);
        query.executeUpdate();
    }
}
