package com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.dao;

import com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class EmployeeDAOJpaImpl implements EmployeeDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Employee> findAll() {
        return entityManager.createQuery("from Employee", Employee.class).getResultList();
    }

    @Override
    public Employee findById(int id) {
        return entityManager.find(Employee.class, id);
    }

    @Override
    public void save(Employee employee) {
        employee.setId(entityManager.merge(employee).getId());
    }

    @Override
    public void deleteById(int id) {
        Query query = entityManager.createQuery("DELETE  from Employee where id=:employeeId");
        query.setParameter("employeeId", id);
        query.executeUpdate();
    }
}
