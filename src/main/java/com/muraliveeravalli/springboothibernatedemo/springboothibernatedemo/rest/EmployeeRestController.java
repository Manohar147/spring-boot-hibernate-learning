//package com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.rest;
//
//import com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.entity.Employee;
//import com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.service.EmployeeService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("/api")
//public class EmployeeRestController {
//
//    @Autowired
//    private EmployeeService employeeService;
//
//
//    @GetMapping("/employees")
//    public List<Employee> findAll() {
//        return employeeService.findAll();
//    }
//
//    @GetMapping("/employees/{employeeId}")
//    public Employee findAll(@PathVariable int employeeId) {
//        Employee employee = employeeService.findByid(employeeId);
//        if (employee == null) {
//            throw new RuntimeException("Employee id not found for id :: " + employeeId);
//        }
//        return employee;
//    }
//
//    @PostMapping("/employees")
//    public Employee saveEmployee(@RequestBody Employee employee) {
//        //setting id to 0 will force hibernate to save the object
//        employee.setId(0);
//        employeeService.save(employee);
//        return employee;
//    }
//
//    @PutMapping("/employees")
//    public Employee updateEmployee(@RequestBody Employee employee) {
//        employeeService.save(employee);
//        return employee;
//    }
//
//    @DeleteMapping("/employees/{employeeId}")
//    public String deleteEmployee(@PathVariable int employeeId) {
//        Employee employee = employeeService.findByid(employeeId);
//        if (employee == null) {
//            throw new RuntimeException("Employee id not found for id :: " + employeeId);
//        }
//        employeeService.deleteById(employeeId);
//        return "Deleted Employee with Id :: " + employeeId;
//
//    }
//
//
//}
