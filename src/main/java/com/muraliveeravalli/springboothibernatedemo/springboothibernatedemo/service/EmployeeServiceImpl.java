package com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.service;

import com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.dao.CustomRepository;
import com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private CustomRepository employeeDao;

    @Override
    public List<Employee> findAll() {
        return employeeDao.findAll();
    }

    @Override
    public Employee findByid(int id) {
        Optional<Employee> result = employeeDao.findById(id);
        if (!result.isPresent()) {
            throw new RuntimeException("Did not find employee id -- " + id);
        }
        return result.get();
    }

    @Override
    public void save(Employee employee) {
        employeeDao.save(employee);
    }

    @Override
    public void deleteById(int id) {
        employeeDao.deleteById(id);
    }
}
