package com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.service;

import com.muraliveeravalli.springboothibernatedemo.springboothibernatedemo.entity.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> findAll();

    Employee findByid(final int id);

    void save(final Employee employee);

    void deleteById(final int id);

}
